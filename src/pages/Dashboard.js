import classes from '../css/Dashboard.module.css';
import Navigation from '../components/Navigation'
import Header from '../components/Header';

function Dashboard()  {
  return (
    <div className={classes.container}>
      <Header text="Dashboard" />
      <Navigation />
    </div>
  );
}

export default Dashboard;
