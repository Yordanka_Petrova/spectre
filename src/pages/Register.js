import { useRef } from 'react';
import { Link, useHistory } from 'react-router-dom';
import Button from '../components/Button';
import Header from '../components/Header';
import classes from '../css/UserService.module.css';
import { createUser } from '../api/users.js';

function Register() {
    let history = useHistory();
  
    const usernameInputRef = useRef();
    const passwordInputRef = useRef();
    const confirmPasswordInputRef = useRef();
   
    function handleSubmit(event) {
      event.preventDefault();
  
      const enteredUsername = usernameInputRef.current.value;
      const enteredPassword = passwordInputRef.current.value;
      const enteredConfirmPassword = confirmPasswordInputRef.current.value;
  
      if(enteredPassword !== enteredConfirmPassword)
        alert("Password and confirm password are not the same!");
      else {
        const newUser = {
          username: enteredUsername,
          password: enteredPassword
        };

        createUser(newUser)
          .then(() => {
              history.replace("/login");
          });
      }
    }
  
    return (
        <div className={classes.container}>
          <Header text="Register" />
            <form onSubmit={handleSubmit}>
              <div>
                <label htmlFor="username">Username</label>
                <input className={classes.input} type="text" placeholder="Enter Username" name="username" ref={usernameInputRef} required/>
  
                <label htmlFor="password">Password</label>
                <input className={classes.input} type="password" placeholder="Enter Password" name="password" ref={passwordInputRef} required />
  
                <label htmlFor="confirmPassword">Confirm Password</label>
                <input className={classes.input} type="password" placeholder="Confirm Password" name="confirmPassword" ref={confirmPasswordInputRef} required />
              </div>
              <Button text="Register" type="submit" />
              <p>Already have an account? <span> <Link to='/login'>Login</Link></span></p>
            </form>
        </div>
    );
  }

export default Register;
