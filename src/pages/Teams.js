import classes from '../css/Dashboard.module.css';
import Header from '../components/Header';
import Navigation from '../components/Navigation'
import Button from '../components/Button';
import Popup from '../components/Popup';
import { useEffect, useState } from 'react';
import { getTeams } from '../api/teams.js';
import Team from '../components/Team';

function Teams()  {
  const [popupIsOpen, setPopupIsOpen] = useState(false);
  const [teams, setTeams] = useState([]);

  function listTeams() {
    getTeams()
    .then(teams => {
      setTeams(teams);
    });
  }

  function handlePopup() {
    setPopupIsOpen(true);
  }

  function handleClose() {
    setPopupIsOpen(false);
  }

  useEffect(() => {
    listTeams();
  }, []);

  return (
    <div className={classes.container}>
      <Header text="Teams" />
      <Navigation />
      <Button text="Add team" type="button" onClick={handlePopup}/>
      {popupIsOpen ? <Popup listTeams={listTeams} closePopup={handleClose.bind(this)} /> : null}
      {teams.map((team, key) => {
        return <Team key={key} team={team} listTeams={listTeams}/>
      })}
    </div>
  );
}

export default Teams;
