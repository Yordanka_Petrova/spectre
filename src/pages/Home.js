import { Link } from 'react-router-dom';
import Button from '../components/Button';
import Header from '../components/Header';
import classes from '../css/Home.module.css';

function Home()  {

  return (
      <div className={classes.container}>
        <Header text="Home" />
        <Link style={{textDecoration: "none", color: "white"}} to='/register'><Button text="Register" type="button" /></Link>
        <Link style={{textDecoration: "none", color: "white"}} to='/login'><Button text="Login" type="button" /></Link>
      </div>
  );
}

export default Home;
