import { useRef } from 'react';
import { Link, useHistory } from 'react-router-dom';
import Button from '../components/Button';
import Header from '../components/Header';
import classes from '../css/UserService.module.css';
import { getUser } from '../api/users.js';

function Login()  {
  let history = useHistory();

  const usernameInputRef = useRef();
  const passwordInputRef = useRef();

  function handleSubmit(event) {
    event.preventDefault();

    const enteredUsername = usernameInputRef.current.value;
    const enteredPassword = passwordInputRef.current.value;

    getUser(enteredUsername)
      .then(user => {
          user = user[0];
          if(user.password === enteredPassword) {
             localStorage.setItem('username', enteredUsername);
             history.replace('/dashboard');
          }
          else {
            alert("Incorrect password!");
          }
        })
      .catch(() => {
        alert("User does not exist!");
      });
  }

  return (
    <div className={classes.container}>
      <Header text="Login" />
        <form onSubmit={handleSubmit}>
          <div>
            <label htmlFor="username">Username</label>
            <input className={classes.input} type="text" placeholder="Enter Username" name="username" ref={usernameInputRef} required />

            <label htmlFor="password">Password</label>
            <input className={classes.input} type="password" placeholder="Enter Password" name="password" ref={passwordInputRef} required />
            
            <Button text="Login" type="submit"/>
            <p>Don't have an account? <span> <Link to='/register'>Register</Link></span></p>
          </div>
        </form>      
    </div>
  );
}

export default Login;