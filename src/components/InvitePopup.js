import { useEffect, useState } from 'react';
import { getUsers } from '../api/users';
import { updateTeam } from '../api/teams';
import classes from '../css/Popup.module.css';
import Button from './Button';
import Header from './Header';

function InvitePopup(props)  {
  const [users, setUsers] = useState([]);
  const [user, setUser] = useState("");

  useEffect(() => {
    getUsers()
    .then(users => {
      setUsers(users);
  });
  }, []);

  function handleChange(event) {
    setUser(event.target.value);
  }

  function handleInvite(event) {
    event.preventDefault();
    updateTeam(props.teamId, user).then(() => {
      props.listMembers(props.teamId);
      props.closePopup();
    });
  }

  return (
      <div className={classes.popup}>
        <div className={classes.popupInner}>
          <Header text="Invite member" />
          <label htmlFor="members">Choose a member: </label>
          <select value={user} onChange={handleChange}>
            <option> -- select an option -- </option>
            {users.map((user, key) => {
              return <option value={user.value} key={key}>{user.username}</option>
            })}
          </select>
          <Button text="Invite" type="button" onClick={handleInvite}/>
          <Button text="Close" type="button" onClick={props.closePopup} />
        </div>
      </div>
  );
}

export default InvitePopup;
