import classes from '../css/Team.module.css';
import Header from "./Header";
import Button from "./Button";
import InvitePopup from "./InvitePopup";
import { useEffect, useState } from 'react';
import { deleteMember, deleteTeam, getMembers } from '../api/teams';

function Team(props)  {
  const [invitePopupIsOpen, setInvitePopupIsOpen] = useState(false);
  const [members, setMembers] = useState([]);

  function listMembers(teamId) {
    getMembers(teamId)
    .then(members => {
      setMembers(members);
    });
  }

  function handleOpenPopup() {
    setInvitePopupIsOpen(true);
  }

  function handleClose() {
    setInvitePopupIsOpen(false);
  }

  function handleDeleteMember(member) {
    deleteMember(member, props.team.id).then(() => {
      listMembers(props.team.id);
    });
  }

  function handleDeleteTeam() {
    deleteTeam(props.team.id).then(() => {
      props.listTeams();
    })
  }

  useEffect(() => {
    listMembers(props.team.id);
  }, [props.team.id]);

  return (
      <div className={classes.container}>
          <div className={classes.team}>
            <Header text={props.team.name} />
            <button className={classes.button} type="button" onClick={handleDeleteTeam}>x</button>
          </div>
          <Button text="Invite member" type="button" onClick={handleOpenPopup} />
          {invitePopupIsOpen ? <InvitePopup teamId={props.team.id} listMembers={listMembers} closePopup={handleClose.bind(this)} /> : null}
          <div className={classes.div}>
            {members.map((member, key) => {
              return <div className={classes.div} key={key}>
                        <p>{member}</p>
                        <button className={classes.button} type="button" onClick={() => handleDeleteMember(member)}>x</button>
                    </div>
            })}
          </div>
      </div>
  );
}

export default Team;
