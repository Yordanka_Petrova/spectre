import { useHistory } from 'react-router-dom';
import Button from '../components/Button';

function Navigation()  {
  let history = useHistory();

  function handleLogout() {
    localStorage.removeItem('username');
    history.replace('/login');
  }

  function handleTeams() {
    history.replace('/teams');
  }

  return (
      <div style={{display: "flex"}}>
        <Button text="Teams" type="button"  onClick={handleTeams} />
        <Button text="Boards" type="button"  />
        <Button text="Statistics" type="button" />
        <Button text="Logout" type="button" onClick={handleLogout} />
      </div>
  );
}

export default Navigation;
