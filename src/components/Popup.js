import { useRef } from 'react';
import { createTeam } from '../api/teams';
import classes from '../css/Popup.module.css';
import Button from './Button';
import Header from './Header';

function Popup(props)  {
  const teamNameInputRef = useRef();
  
  function handleSubmit(event) {
    event.preventDefault();

    const enteredTeamName = teamNameInputRef.current.value;
    const newTeam = {
      "name": enteredTeamName,
      "members": []
    }
    createTeam(newTeam).then(() => {
      props.closePopup();
      props.listTeams();
    });
  }

  return (
      <div className={classes.popup}>
        <div className={classes.popupInner}>
          <Header text="Create team" />
          <form onSubmit={handleSubmit}>
              <div>
                <label htmlFor="teamName">Team name</label>
                <input className={classes.input} type="text" placeholder="Enter team name" name="teamName" ref={teamNameInputRef} required/><br />
              </div>
              <Button text="Add" type="submit" />
          </form>
          <Button text="Close" type="button" onClick={props.closePopup} />
        </div>
      </div>
  );
}

export default Popup;
