import classes from '../css/Button.module.css';

function Button(props)  {
  return (
      <button className={classes.button} type={props.type} onClick={props.onClick}>{props.text}</button>
  );
}

export default Button;
