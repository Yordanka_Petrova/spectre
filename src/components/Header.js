import classes from '../css/Header.module.css';

function Header(props)  {
  return (
      <div className={classes.header}>{props.text}</div>
  );
}

export default Header;
