export function getTeams() {
    const url = `http://localhost:8000/teams`;
    return fetch(url).then(res => res.json());
}

export function getMembers(teamId) {
  return getTeam(teamId).then(team => {
    return team.members;
  })
}

export function deleteMember(member, teamId) {
  const url = `http://localhost:8000/teams/${teamId}`;
  return getTeam(teamId).then(team => {
    const index = team.members.indexOf(member);
    team.members.splice(index, 1);

    return fetch(url, {
      method: 'PATCH',
      body: JSON.stringify(team),
      headers: {'Content-Type': 'application/json'}
    })
  }) 
}

export function getTeam(teamId) {
  const url = `http://localhost:8000/teams/${teamId}`;
  return fetch(url).then(res => res.json());
}

export function createTeam(newTeam) {
    const url = `http://localhost:8000/teams`;
    return fetch(url, {
              method: 'POST',
              body: JSON.stringify(newTeam),
              headers: {'Content-Type': 'application/json'}
            })
}

export function deleteTeam(teamId) {
  const url = `http://localhost:8000/teams/${teamId}`;
  return fetch(url, {
    method: 'DELETE',
    headers: {'Content-Type': 'application/json'}
  })
}

export function updateTeam(teamId, newMember) {
  const url = `http://localhost:8000/teams/${teamId}`;
  return getTeam(teamId).then(team => {
    team.members.push(newMember);
  
    return fetch(url, {
      method: 'PATCH',
      body: JSON.stringify(team),
      headers: {'Content-Type': 'application/json'}
    })
  })
}