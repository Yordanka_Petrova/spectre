
export function getUser(username) {
  const url = `http://localhost:8000/users?username=${username}`;
  return fetch(url).then(res => res.json());
}

export function createUser(newUser) {
  const url = `http://localhost:8000/users`;
  return fetch(url, {
            method: 'POST',
            body: JSON.stringify(newUser),
            headers: {'Content-Type': 'application/json'}
          })
}

export function getUsers() {
  const url = `http://localhost:8000/users`;
  return fetch(url).then(res => res.json());
}