import { Route, Switch } from 'react-router-dom';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Dashboard from './pages/Dashboard';
import Teams from './pages/Teams';
import ProtectedRoute from './pages/ProtectedRoute';
import classes from './css/App.module.css';

function App() {
  return (
    <div>
      <h1 className={classes.header}>Spectre: Retrospective Meeting Platform</h1>

      <Switch>
        <Route path='/' exact={true}>
          <Home />
        </Route>
        <Route path='/login' exact={true}>
          <Login />
        </Route>
        <Route path='/register' exact={true}>
          <Register />
        </Route>
        <ProtectedRoute path='/dashboard' component={Dashboard} exact={true} />
        <ProtectedRoute path='/teams' component={Teams} exact={true} />
      </Switch>
    </div>
  );
}

export default App;
